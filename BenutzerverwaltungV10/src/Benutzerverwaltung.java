import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzerverwaltung {

	 public static void main(String[] args) {

		 BenutzerverwaltungV10.start(); 
	}
}
	
	

class BenutzerverwaltungV10 {	
	    public static void start(){
	        BenutzerListe benutzerListe = new BenutzerListe();
	        Scanner tastatur = new Scanner(System.in);
	        
	        benutzerListe.insert(new Benutzer("Paula", "paula"));
	        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
	        benutzerListe.insert(new Benutzer("Darko", "darko"));

	        // Hier bitte das Menü mit der Auswahl
	        //  - Anmelden
	        //  - Registrieren
	        // einfügen, sowie die entsprechenden Abläufe:
	        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
	        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
	        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
	        // in der Liste nach dem Namen suchen und das eingegebene Passwort
	        // mit dem gespeicherten vergleichen.
	        
	        System.out.println("Was m�chten Sie tun?\n");
	        System.out.println("Anmelden (1)");
	        System.out.println("Registrieren (2)");
	        System.out.print("Ihre Auswahl:");
	        
	        int auswahl = tastatur.nextInt();
	        
	        if (auswahl == 1) {
	        	anmelden(tastatur, benutzerListe);
	        } else if (auswahl == 2) {
	        	registrieren(tastatur, benutzerListe);
	        }

	        System.out.println(benutzerListe.select());
	        System.out.println(benutzerListe.select("Peter"));
	        System.out.println(benutzerListe.select("Darko"));
	    }
	    
	    public static void anmelden(Scanner tastatur, BenutzerListe benutzerListe) {
	    	String name;
	    	String passwort;
	    	
	    	int versuche = 0;
	    	
	    	while (versuche < 3) {
	    		System.out.print("Bitte geben Sie Ihren Benutzernamen ein:");
	    		name = tastatur.next();
	    		
	    		System.out.print("Bitte geben Sie Ihr Passwort ein:");
	    		passwort = tastatur.next();
	    		
	    		Benutzer benutzer = benutzerListe.sucheBenutzer(name);
	    		
	    		if (benutzer == null || !passwort.equals(benutzer.getPasswort())) {
	    			System.out.println("Falscher Name oder falsches Passwort");
	    			name = null;
	    			passwort = null;
	    			versuche++;
	    		}else {
	    			System.out.println("Anmeldung erfolgreich!");
	    			benutzer.setAnmeldestatus("online");
	    			Date date = new Date();
	    			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
	    			benutzer.setLetzterLogin(formatter.format(date));
	    			return;
	    		}
	    	}
	    	System.out.println("Anmeldung fehlgeschlagen.");
	    }
	    
	    public static void registrieren(Scanner tastatur, BenutzerListe benutzerListe) {
	    	String name;
	    	String passwort;
	    	String passwortBest�tigung;
	    	boolean nutzerBereitsVorhanden = false;
	    	
	    	do {
	    		System.out.print("Bitte geben Sie Ihren gew�nschten Nutzernamen ein:");
	    		name = tastatur.next();
	    		
	    		if (benutzerListe.sucheBenutzer(name) != null) {
	    			System.out.println("Nutzername bereits vorhanden.");
	    			nutzerBereitsVorhanden = true;
	    		} else {
	    			boolean passw�rterSindUngleich = true;
	    			nutzerBereitsVorhanden = false;
	    			
	    			do {
	    				System.out.print("Bitte geben Sie Ihr gew�nschtes Passwort ein:");
		    			passwort = tastatur.next();
		    			
		    			System.out.print("Bitte best�tigen Sie Ihr Passwort");
		    			passwortBest�tigung = tastatur.next();
		    			
		    			if (passwort.equals(passwortBest�tigung)) {
		    				benutzerListe.insert(new Benutzer(name, passwort));
		    				System.out.println("Sie wurden erfolgreich registriert, " + name + ".");
		    				passw�rterSindUngleich = false;
		    			} else {
		    				System.out.println("Die Passw�rter stimmen nicht �berein.");
		    			}
		    			
	    			} while (passw�rterSindUngleich);	
	    		}
	    	} while (nutzerBereitsVorhanden);	
	    }
	}
class BenutzerListe{
	    private Benutzer first;
	    private Benutzer last;
	    
	    public BenutzerListe(){
	        first = last = null;
	    }
	    public void insert(Benutzer b){
	        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name){
        // ...
        return true;
    }
    
    public Benutzer sucheBenutzer(String name) {
    	 Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b;
            }
            b = b.getNext();
        }
        return null;
    }
}
class Benutzer{
	    private String name;
	    private String passwort;
	    private String anmeldestatus;
	    private String letzterLogin;
	    private Benutzer next;

	    public Benutzer(String name, String pw){
	        this.name = name;
	        this.passwort = pw;

	        this.next = null;
	    }

	    public boolean hasName(String name){
	        return name.equals(this.name);
	    }

	    public String toString(){
	        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
    
    public String getPasswort() {
    	return passwort;
    }
    
    public String getAnmeldestatus() {
    	return anmeldestatus;
    }
    public void setAnmeldestatus(String status) {
    	anmeldestatus = status;
    }
    
    public String getLetzterLogin() {
    	return letzterLogin;
    }
    
    public void setLetzterLogin(String datum) {
    	letzterLogin = datum;
    }
}
